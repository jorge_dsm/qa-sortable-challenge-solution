FROM ubuntu

# Install needed tools
RUN apt-get update && apt-get install -y gnupg wget unzip

# Install ruby
run apt-get install -y ruby ruby-dev

# Install selenium-webdriver
RUN apt-get install -y gcc make libffi-dev && gem install --no-document watir

# Firefox browser to run the tests
RUN apt-get install -y firefox

# Gecko Driver
ENV GECKODRIVER_VERSION 0.23.0
RUN wget https://github.com/mozilla/geckodriver/releases/download/v$GECKODRIVER_VERSION/geckodriver-v$GECKODRIVER_VERSION-linux64.tar.gz \
  && tar -zxf geckodriver-v$GECKODRIVER_VERSION-linux64.tar.gz && rm geckodriver-v$GECKODRIVER_VERSION-linux64.tar.gz \
  && mv geckodriver /opt/geckodriver && chmod 755 /opt/geckodriver \
  && ln -fs /opt/geckodriver /usr/bin/geckodriver \
  && ln -fs /opt/geckodriver /usr/bin/wires

# Chrome browser to run the tests
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
    && echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list \
    && apt-get update && apt-get -y install google-chrome-stable \
    && rm /etc/apt/sources.list.d/google.list

# Chrome Driver
ENV CHROMEDRIVER_VERSION 2.43
RUN wget http://chromedriver.storage.googleapis.com/$CHROMEDRIVER_VERSION/chromedriver_linux64.zip \
  && unzip chromedriver_linux64.zip && rm chromedriver_linux64.zip \
  && mv chromedriver /opt/chromedriver && chmod 755 /opt/chromedriver \
  && ln -fs /opt/chromedriver /usr/bin/chromedriver

# entrypoint
COPY . .
ENTRYPOINT ["/usr/bin/ruby", "run.rb"]
