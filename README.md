# QA Sortable Challenge

Hello, this is my solution for the Mindera QA Sortable challenge.


### Build and Install
On the root of the project run:
```
docker build . -t sortable
```
Now to run a test you must already have the app running on http://localhost:3000

### Running Tests
To run the basic test use the command:
```
docker run --network host sortable
```

The test script supports some arguments so you can customize the test:
```
-b [BROWSER]  Browser to use on the test [firefox|chrome]
-u [URL]      URL where the target app is running
-o [ORDER]    Ordering of the list [ascending|descending]
-s            Take screenshots at start and end of the test
```

##### Examples:
Run the test on a chrome browser:
```
docker run --network host sortable -b chrome
```
Order the list in descending order (Bottom is smallest)
```
docker run --network host sortable -o descending
```
Save screenshots to the root of the image
```
docker run --network host sortable -s
```

### Tools and Frameworks
This docker image is based on ubuntu. It has Firefox, Chrome and their correspondent drivers installed.

The test script is written in ruby and uses the gem watir (wrapper for the selenium-webdriver) for browser handling.