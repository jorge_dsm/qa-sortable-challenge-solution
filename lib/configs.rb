require 'yaml'

def load_configs
  configs = YAML::load_file("#{__dir__}/../config/conf.yml")
  Log.debug('Configs were loaded successfully')
  configs
end