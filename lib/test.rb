# browser libs
require 'watir'

# internal libs
require_relative 'configs'
require_relative 'logger'

module Test
  extend self

  CONFIGS = load_configs

  CONNECTION_MESSAGE = 'An error ocurred while connecting to the App. Is the App running?'
  TIMEOUT_MESSAGE = 'An error occurred while waiting for the App container'
  OUT_OF_ORDER = 'A list item is out of order: %s'

  # Sets the arguments of the test
  # @param properties [Hash]
  def set_properties(properties={})
    Log.debug("Test arguments: #{properties.inspect}")

    @which = (properties[:browser] || CONFIGS[:selenium][:browser]).to_s.strip.downcase.to_sym
    @url = properties[:url] || CONFIGS[:app][:url]
    @order = properties[:order] || 'ascending'
    @screenshot = properties[:screenshot]
  end

  # Runs the test
  # @note call set_properties first
  def run!
    # start the browser
    start

    container = @browser.div(id: 'app')

    # lets wait for the app to load
    begin
      container.wait_until_present
    rescue Exception => e
      Log.error(TIMEOUT_MESSAGE)
      raise(e)
    end

    # sort the list items
    Log.info('Starting to sort list items')

    send("sort_#{@order}", container)

    Log.info('Finished sorting list items')

  rescue Exception => e
    finish
    raise(e)
  end

  # Checks if the test ordered the list correctly
  def check
    container = @browser.div(id: 'app')

    index = @order == 'ascending' ? 0 : container.lis.size - 1

    Log.info("Checking if the list is in #{@order} order")
    container.lis.each do |li|
      li_number = li.text[/\d+$/].to_i
      raise(OUT_OF_ORDER % li.text) unless li_number == index
      @order == 'ascending' ? index += 1 : index -= 1
    end

    Log.info("The list is correctly ordered in #{@order} order!")
  ensure
    finish
  end

  private

  # Capabilities to initiate the driver with
  def caps
    {headless: true, switches: CONFIGS[:selenium][:flags][@which]}
  end

  # Starts the Driver/Browser in headless mode
  # Also takes a screenshot and starts a recording if properties are set
  def start
    # start the browser/driver
    Log.info("Openning a #{@which.to_s} browser")
    @browser = Watir::Browser.new(@which, caps)
    
    # Navigate to the app
    Log.debug("Navigating to the App at #{@url}")
    begin
      @browser.goto(@url)
    rescue Exception => e
      Log.error(CONNECTION_MESSAGE)
      raise(e)
    end

    if @screenshot
      Log.debug('Saving screenshot of the starting state of the App to file \'test_image_start.png\'')
      @browser.driver.save_screenshot('test_image_start.png')
    end
  end

  # Closes the Driver/Browser
  # Also stops pending recordings and takes a screenshot if properties are set
  def finish
    if @browser
      if @screenshot
        Log.debug('Saving screenshot of the end state of the App to file \'test_image_end.png\'')
        @browser.driver.save_screenshot('test_image_end.png')
      end
      
      Log.info("Closing the #{@which.to_s} browser")  
      @browser.close
    end
  end

  # Sort the list items in ascending order (Top is smallest)
  def sort_ascending(container)
    (0..5).each do |i|
      Log.debug("Moving Item #{i} to the bottom")
      container.li(text: "Item #{i}").drag_and_drop_on(container.lis.last)
    end
  end

  # Sort the list items in descending order (Bottom is smallest)
  def sort_descending(container)
    (0..5).each do |i|
      Log.debug("Moving Item #{i} to the top")
      container.li(text: "Item #{i}").drag_and_drop_on(container.lis.first)
    end
  end
end
