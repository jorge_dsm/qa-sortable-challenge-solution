require 'optparse'

# parse the arguments
options = {}
OptionParser.new do |opts|
  opts.banner = 'Usage: run.rb [options]'

  opts.on('-b', '--browser [BROWSER]', String, 'Browser to use on the test [firefox|chrome]'){ |b| options[:browser] = b }
  opts.on('-u', '--url [URL]', String, 'URL where the target app is running'){ |u| options[:url] = u }
  opts.on('-o', '--order [ORDER]', [:ascending, :descending], 'Ordering of the list'){ |o| options[:order] = o }
  opts.on('-s', '--screenshot', 'Take screenshots at start and end of the test'){ options[:screenshot] = true }
  opts.on_tail('-h', '--help', 'Show this message') { puts opts; exit }
end.parse!

# import test module
require_relative 'lib/test'

# set test arguments
Test.set_properties(options)

# run test
Test.run!

# check result
Test.check
